from src.qollib.strings.numeric import *


def test_num_compare():
    assert -1 == num_compare('foo42.bla', 'bar1337')
    assert 1 == num_compare('bar1337', 'foo42.bla')
    assert 0 == num_compare('foo42.bla', 'foo42.bla')


def test_extract_int():
    assert 1337 == extract_int('foo1337bar')
    assert 42 == extract_int('foo42bar1337baz')
    assert 0 == extract_int('foo')
    assert 0 == extract_int('')


def test_sort_num():
    result = sort_num(['third123a', 'first12b', 'second050c'])
    assert result[0] == 'first12b'
    assert result[1] == 'second050c'
    assert result[2] == 'third123a'
