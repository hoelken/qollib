from src.qollib.processing.thread import Thread


def test_initial_status():
    thread = Thread(func, 'foo')

    assert thread.future is None
    assert thread.function == func
    assert not thread.is_submitted()
    assert not thread.is_done()
    assert thread.result is None


def test_submitting(mocker):
    executer = mocker.patch('concurrent.futures.Executor')
    future = mocker.patch('concurrent.futures.Future')
    executer.submit.return_value = future
    future.done.return_value = True
    thread = Thread(func, 'foo')

    thread.submit(executer)
    assert executer.submit.called
    assert thread.is_submitted()
    assert thread.is_done()


def test_cancel(mocker):
    thread = Thread(func, 'foo')
    thread.future = mocker.patch('concurrent.futures.Future')
    thread.future.done.return_value = False

    assert thread.is_submitted()
    assert not thread.is_done()

    thread.cancel()
    assert thread.future.cancel.called


def test_get_result(mocker):
    thread = Thread(func, 'foo')
    assert thread.result is None

    thread.future = mocker.patch('concurrent.futures.Future')
    thread.future.done.return_value = True
    thread.future.result.return_value = 42

    assert thread.result == 42


def test_exception(mocker):
    thread = Thread(func, 'foo')
    thread.future = mocker.patch('concurrent.futures.Future')
    thread.future.done.return_value = True
    thread.future.exception.return_value = RuntimeError

    assert thread.is_done()
    assert thread.exception == RuntimeError


def func(arg):
    pass
