
from src.qollib.processing.testing import sequentially


def test_threaded_result():
    result = sequentially(divide_by, [1, 2, 4])
    result = sorted(result)
    assert result[2] == 1.0
    assert result[1] == 0.5
    assert result[0] == 0.25


def divide_by(num):
    return 1 / num
