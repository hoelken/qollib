from unittest.mock import MagicMock, call

from src.qollib.ui.progress import *


def test_print_green_dot(mocker):
    printer = mocker.patch('builtins.print')
    dot()
    _assert_message_printed(printer, f'{GREEN}*{RESET}', end='', flush=True)


def test_print_red_dot(mocker):
    printer = mocker.patch('builtins.print')
    dot(False, char='.')
    _assert_message_printed(printer, f'{RED}.{RESET}', end='', flush=True)


def test_dot_flush(mocker):
    printer = mocker.patch('builtins.print')
    dot(flush=True)
    _assert_message_printed(printer, '')


def test_msg(mocker):
    printer = mocker.patch('builtins.print')
    msg('Test foo')
    _assert_message_printed(printer, 'Test foo', end='\r', flush=True)


def test_msg_flush(mocker):
    printer = mocker.patch('builtins.print')
    msg(flush=True)
    _assert_message_printed(printer, '')


def test_bar(mocker):
    printer = mocker.patch('builtins.print')
    bar(10, 100)
    _assert_message_printed(printer, '┣━━━━━                                             ┃ \t[10.00 %]',
                            end='\r', flush=True)
    bar(100, 100, flush=True)
    _assert_message_printed(printer, '┣━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫ \t[100.00 %]',
                            end='\r', flush=True)


def _assert_message_printed(printer: MagicMock, message: str, **kwargs) -> None:
    assert call(message, **kwargs) in printer.mock_calls
