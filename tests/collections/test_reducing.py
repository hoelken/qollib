from src.qollib.collections import flatten, flatten_sorted


def test_flatten():
    seq = [['Hello', 'my'], 'name', [['is', 1337], 42], {'foo': 'bar'}]
    result = flatten(seq)
    assert len(result) == 7
    assert result == ['Hello', 'my', 'name', 'is', 1337, 42, {'foo': 'bar'}]


def test_sorted_flatten():
    seq = [[1, 3], [5], [4, [2, 0]], 6]
    result = flatten_sorted(seq)
    assert len(result) == 7
    assert result == [0, 1, 2, 3, 4, 5, 6]
