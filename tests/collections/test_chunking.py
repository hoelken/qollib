from src.qollib.collections import chunker, indexed_chunks


def test_chunker():
    seq = ['a', 'b', 'c', 'd', '3.141']

    for chunk in chunker(seq, 1):
        assert len(chunk) == 1
    for chunk in chunker(seq, -1):
        assert len(chunk) == 5


def test_indexed_chunker():
    seq = ['a', 'b', 'c', 'd', '3.141']
    chunks = indexed_chunks(seq, 3)
    assert len(chunks) == 2
    assert chunks[0] == ['a', 'b', 'c']
    assert chunks[1] == ['d', '3.141']
